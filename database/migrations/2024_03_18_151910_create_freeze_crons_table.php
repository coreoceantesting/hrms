<?php

use App\Models\FinancialYear;
use App\Models\Ward;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('freeze_crons', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Ward::class)->nullable()->constrained();
            $table->foreignIdFor(FinancialYear::class)->nullable()->constrained();
            $table->integer('month');
            $table->date('from_date');
            $table->date('to_date');
            $table->integer('status')->default('0')->nullable()->comment('1) Run 0) Pending');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('freeze_crons');
    }
};
