<?php

use App\Models\Employee;
use App\Models\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;



return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_statuses', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Employee::class)->nullable()->constrained();
            $table->integer('Emp_Code');
            $table->foreignIdFor(Status::class)->nullable()->constrained();
            $table->string('remark', 50);
            $table->date('applicable_date');
            $table->integer('is_salary_applicable')->default('0')->nullable()->comment('1) Yes 0) No');
            $table->integer('salary_percent')->default('0')->nullable()->comment('1) 25% 2) 50% 3) 75% 4) 100% 0) No');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->foreignId('deleted_by')->nullable()->constrained('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_statuses');
    }
};