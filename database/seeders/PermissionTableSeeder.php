<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $permissions = [
            [
                'id' => 1,
                'name' => 'dashboard.view',
                'group' => 'dashboard',
            ],
            [
                'id' => 2,
                'name' => 'users.view',
                'group' => 'users',
            ],
            [
                'id' => 3,
                'name' => 'users.create',
                'group' => 'users',
            ],
            [
                'id' => 4,
                'name' => 'users.edit',
                'group' => 'users',
            ],
            [
                'id' => 5,
                'name' => 'users.delete',
                'group' => 'users',
            ],
            [
                'id' => 6,
                'name' => 'users.toggle_status',
                'group' => 'users',
            ],
            [
                'id' => 7,
                'name' => 'users.change_password',
                'group' => 'users',
            ],
            [
                'id' => 8,
                'name' => 'roles.view',
                'group' => 'roles',
            ],
            [
                'id' => 9,
                'name' => 'roles.create',
                'group' => 'roles',
            ],
            [
                'id' => 10,
                'name' => 'roles.edit',
                'group' => 'roles',
            ],
            [
                'id' => 11,
                'name' => 'roles.delete',
                'group' => 'roles',
            ],
            [
                'id' => 12,
                'name' => 'roles.assign',
                'group' => 'roles',
            ],
            [
                'id' => 13,
                'name' => 'wards.view',
                'group' => 'wards',
            ],
            [
                'id' => 14,
                'name' => 'wards.create',
                'group' => 'wards',
            ],
            [
                'id' => 15,
                'name' => 'wards.edit',
                'group' => 'wards',
            ],
            [
                'id' => 16,
                'name' => 'wards.delete',
                'group' => 'wards',
            ],
            [
                'id' => 17,
                'name' => 'departments.view',
                'group' => 'departments',
            ],
            [
                'id' => 18,
                'name' => 'departments.create',
                'group' => 'departments',
            ],
            [
                'id' => 19,
                'name' => 'departments.edit',
                'group' => 'departments',
            ],
            [
                'id' => 20,
                'name' => 'departments.delete',
                'group' => 'departments',
            ],
            [
                'id' => 21,
                'name' => 'class.view',
                'group' => 'class',
            ],
            [
                'id' => 22,
                'name' => 'class.create',
                'group' => 'class',
            ],
            [
                'id' => 23,
                'name' => 'class.edit',
                'group' => 'class',
            ],
            [
                'id' => 24,
                'name' => 'class.delete',
                'group' => 'class',
            ],
            [
                'id' => 25,
                'name' => 'bank.view',
                'group' => 'bank',
            ],
            [
                'id' => 26,
                'name' => 'bank.create',
                'group' => 'bank',
            ],
            [
                'id' => 27,
                'name' => 'bank.edit',
                'group' => 'bank',
            ],
            [
                'id' => 28,
                'name' => 'bank.delete',
                'group' => 'bank',
            ],
            [
                'id' => 29,
                'name' => 'financial_year.view',
                'group' => 'financial_year',
            ],
            [
                'id' => 30,
                'name' => 'financial_year.create',
                'group' => 'financial_year',
            ],
            [
                'id' => 31,
                'name' => 'financial_year.edit',
                'group' => 'financial_year',
            ],
            [
                'id' => 32,
                'name' => 'financial_year.delete',
                'group' => 'financial_year',
            ],
            [
                'id' => 33,
                'name' => 'allowance.view',
                'group' => 'allowance',
            ],
            [
                'id' => 34,
                'name' => 'allowance.create',
                'group' => 'allowance',
            ],
            [
                'id' => 35,
                'name' => 'allowance.edit',
                'group' => 'allowance',
            ],
            [
                'id' => 36,
                'name' => 'allowance.delete',
                'group' => 'allowance',
            ],
            [
                'id' => 37,
                'name' => 'deduction.view',
                'group' => 'deduction',
            ],
            [
                'id' => 38,
                'name' => 'deduction.create',
                'group' => 'deduction',
            ],
            [
                'id' => 39,
                'name' => 'deduction.edit',
                'group' => 'deduction',
            ],
            [
                'id' => 40,
                'name' => 'deduction.delete',
                'group' => 'deduction',
            ],
            [
                'id' => 41,
                'name' => 'loan.view',
                'group' => 'loan',
            ],
            [
                'id' => 42,
                'name' => 'loan.create',
                'group' => 'loan',
            ],
            [
                'id' => 43,
                'name' => 'loan.edit',
                'group' => 'loan',
            ],
            [
                'id' => 44,
                'name' => 'loan.delete',
                'group' => 'loan',
            ],
            [
                'id' => 45,
                'name' => 'designation.view',
                'group' => 'designation',
            ],
            [
                'id' => 46,
                'name' => 'designation.create',
                'group' => 'designation',
            ],
            [
                'id' => 47,
                'name' => 'designation.edit',
                'group' => 'designation',
            ],
            [
                'id' => 48,
                'name' => 'designation.delete',
                'group' => 'designation',
            ],
            [
                'id' => 49,
                'name' => 'leaveType.view',
                'group' => 'leaveType',
            ],
            [
                'id' => 50,
                'name' => 'leaveType.create',
                'group' => 'leaveType',
            ],
            [
                'id' => 51,
                'name' => 'leaveType.edit',
                'group' => 'leaveType',
            ],
            [
                'id' => 52,
                'name' => 'leaveType.delete',
                'group' => 'leaveType',
            ],
            [
                'id' => 53,
                'name' => 'employee.view',
                'group' => 'employee',
            ],
            [
                'id' => 54,
                'name' => 'employee.create',
                'group' => 'employee',
            ],
            [
                'id' => 55,
                'name' => 'employee.edit',
                'group' => 'employee',
            ],
            [
                'id' => 56,
                'name' => 'employee.delete',
                'group' => 'employee',
            ],
            [
                'id' => 57,
                'name' => 'pay_scale.view',
                'group' => 'pay_scale',
            ],
            [
                'id' => 58,
                'name' => 'pay_scale.create',
                'group' => 'pay_scale',
            ],
            [
                'id' => 60,
                'name' => 'pay_scale.delete',
                'group' => 'pay_scale',
            ],
            [
                'id' => 61,
                'name' => 'status.view',
                'group' => 'status',
            ],
            [
                'id' => 62,
                'name' => 'status.create',
                'group' => 'status',
            ],
            [
                'id' => 63,
                'name' => 'status.edit',
                'group' => 'status',
            ],
            [
                'id' => 64,
                'name' => 'status.delete',
                'group' => 'status',
            ],
            [
                'id' => 65,
                'name' => 'employee_status.view',
                'group' => 'employee_status',
            ],
            [
                'id' => 66,
                'name' => 'employee_status.create',
                'group' => 'employee_status',
            ],
            [
                'id' => 67,
                'name' => 'employee_status.edit',
                'group' => 'employee_status',
            ],
            [
                'id' => 68,
                'name' => 'employee_status.delete',
                'group' => 'employee_status',
            ],
            [
                'id' => 69,
                'name' => 'employee-salary.view',
                'group' => 'employee-salary',
            ],
            [
                'id' => 70,
                'name' => 'employee-salary.create',
                'group' => 'employee-salary',
            ],
            [
                'id' => 71,
                'name' => 'employee-salary.edit',
                'group' => 'employee-salary',
            ],
            [
                'id' => 72,
                'name' => 'employee-salary.delete',
                'group' => 'employee-salary',
            ],
            [
                'id' => 73,
                'name' => 'basic-salary-increment.view',
                'group' => 'basic-salary-increment',
            ],
            [
                'id' => 74,
                'name' => 'basic-salary-increment.create',
                'group' => 'basic-salary-increment',
            ],
            [
                'id' => 75,
                'name' => 'allowance-increment.view',
                'group' => 'allowance-increment',
            ],
            [
                'id' => 76,
                'name' => 'allowance-increment.create',
                'group' => 'allowance-increment',
            ],
            [
                'id' => 77,
                'name' => 'deduction-increment.view',
                'group' => 'deduction-increment',
            ],
            [
                'id' => 78,
                'name' => 'deduction-increment.create',
                'group' => 'deduction-increment',
            ],
            [
                'id' => 79,
                'name' => 'employee-loans.view',
                'group' => 'employee-loans',
            ],
            [
                'id' => 80,
                'name' => 'employee-loans.create',
                'group' => 'employee-loans',
            ],
            [
                'id' => 81,
                'name' => 'employee-loans.stop',
                'group' => 'employee-loans',
            ],
            [
                'id' => 82,
                'name' => 'employee-monthly-loans.view',
                'group' => 'employee-monthly-loans',
            ],
            [
                'id' => 83,
                'name' => 'employee-monthly-loans.stop',
                'group' => 'employee-monthly-loans',
            ],
            [
                'id' => 84,
                'name' => 'freeze-attendance.view',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 85,
                'name' => 'freeze-attendance.create',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 86,
                'name' => 'freeze-attendance.edit',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 87,
                'name' => 'freeze-attendance.delete',
                'group' => 'freeze-attendance',
            ],
            [
                'id' => 88,
                'name' => 'salary-slips.view',
                'group' => 'salary-slips',
            ],
            [
                'id' => 89,
                'name' => 'bank-statement.view',
                'group' => 'bank-statement',
            ],
            [
                'id' => 90,
                'name' => 'pay-sheet-pdf.view',
                'group' => 'pay-sheet-pdf',
            ],
            [
                'id' => 91,
                'name' => 'bank-deduction-employee-report.view',
                'group' => 'bank-deduction-employee-report',
            ],
            [
                'id' => 92,
                'name' => 'bank-deduction-report.view',
                'group' => 'bank-deduction-report',
            ],
            [
                'id' => 93,
                'name' => 'summary-department-report.view',
                'group' => 'summary-department-report',
            ],
            [
                'id' => 94,
                'name' => 'summary-ward-report.view',
                'group' => 'summary-ward-report',
            ],
            [
                'id' => 95,
                'name' => 'summary-report.view',
                'group' => 'summary-report',
            ],
            [
                'id' => 96,
                'name' => 'pay-sheet-excel.view',
                'group' => 'pay-sheet-excel',
            ],
            [
                'id' => 97,
                'name' => 'allowance-report.view',
                'group' => 'allowance-report',
            ],
            [
                'id' => 98,
                'name' => 'deduction-report.view',
                'group' => 'deduction-report',
            ]
        ];

        foreach ($permissions as $permission) {
            Permission::updateOrCreate([
                'id' => $permission['id']
            ], [
                'id' => $permission['id'],
                'name' => $permission['name'],
                'group' => $permission['group']
            ]);
        }
    }
}
