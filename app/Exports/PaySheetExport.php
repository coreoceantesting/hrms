<?php

namespace App\Exports;

use App\Models\FreezeAttendance;
use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\EmployeeMonthlyLoan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PaySheetExport implements FromCollection, WithHeadings
{
    protected $freezeAttendanceData;

    public function __construct(Collection $freezeAttendanceData)
    {
        $this->freezeAttendanceData = $freezeAttendanceData;
    }

    public function collection()
    {
        // Add allowance headings dynamically
        $allowances = Allowance::get();
        $deductions = Deduction::get();

        $data = [];
        $grand_total_basic_salary = 0;
        $grand_total_earn = 0;
        $allowanceTotals = array_fill_keys($allowances->pluck('id')->toArray(), 0);
        $deductionTotals = array_fill_keys($deductions->pluck('id')->toArray(), 0);

        $grand_total_pf = 0;
        $grand_total_bank_loan = 0;
        $grand_total_stamp_duty = 0;
        $grand_total_deductions = 0;
        $grand_total_net_salary = 0;
        $grand_total_corporation_share = 0;

        foreach ($this->freezeAttendanceData as $key => $freeze_attendance) {
            $explode_allowance_ids = explode(',', $freeze_attendance->allowance_Id);
            $explode_allowance_amt = explode(',', $freeze_attendance->allowance_Amt);
            $explode_deduction_ids = explode(',', $freeze_attendance->deduction_Id);
            $explode_deduction_amt = explode(',', $freeze_attendance->deduction_Amt);
            $explode_loan_ids = explode(',', $freeze_attendance->loan_deduction_id);

            // Grand Total Calculations
            $grand_total_basic_salary += $freeze_attendance->basic_salary;
            $grand_total_earn += ($freeze_attendance->basic_salary + $freeze_attendance->total_allowance);

            $rowData = [
                $key + 1, // Sr No.
                $freeze_attendance->Emp_Code,
                $freeze_attendance->present_day,
                $freeze_attendance->emp_name,
                $freeze_attendance->pay_band_scale . " " . $freeze_attendance->grade_pay_scale,
                $freeze_attendance->date_of_appointment,
                optional($freeze_attendance->designation)->name,
                $freeze_attendance->basic_salary,
            ];

            // Process allowances and calculate totals
            foreach ($allowances as $allowance) {
                $index = array_search($allowance->id, $explode_allowance_ids);
                $allowanceAmt = $index !== false ? $explode_allowance_amt[$index] : 0;
                $rowData[] = $allowanceAmt;
                $allowanceTotals[$allowance->id] += $allowanceAmt;
            }

            // Add total earning
            $rowData[] = $freeze_attendance->basic_salary + $freeze_attendance->total_allowance;

            // Process deductions and calculate totals
            foreach ($deductions as $deduction) {
                $index = array_search($deduction->id, $explode_deduction_ids);
                $deductionAmt = $index !== false ? $explode_deduction_amt[$index] : 0;
                $rowData[] = $deductionAmt;
                $deductionTotals[$deduction->id] += $deductionAmt;
            }

            // Calculate PF Loan and Bank Loan
            $pf_amt = 0;
            $bank_loan = 0;
            if ($freeze_attendance->loan_deduction_id) {
                foreach ($explode_loan_ids as $loan_id) {
                    $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first();
                    if ($emp_loan && $emp_loan->loan_id == 1) {
                        $pf_amt = $emp_loan->installment_amount;
                        $grand_total_pf += $pf_amt;
                    }
                    $bank_loan = $freeze_attendance->total_loan_deduction - $pf_amt;
                }
            }

            $grand_total_bank_loan += $bank_loan;
            $grand_total_stamp_duty += $freeze_attendance->stamp_duty;

            $grand_total_deductions += $freeze_attendance->total_deduction;

            $grand_total_net_salary += $freeze_attendance->net_salary;
            $grand_total_corporation_share += $freeze_attendance->corporation_share_da;

            $rowData[] = $pf_amt;
            $rowData[] = $bank_loan;
            $rowData[] = $freeze_attendance->stamp_duty;
            $rowData[] = $freeze_attendance->total_deduction;
            $rowData[] = $freeze_attendance->net_salary;
            $rowData[] = $freeze_attendance->corporation_share_da;
            $rowData[] = optional($freeze_attendance->employee->employee_status)->remark ?? $freeze_attendance->remark;

            $data[] = $rowData;
        }

        // Add grand total row with allowance totals
        $grandTotalRow = ['Grand Total:', '', '', '', '', '', '', $grand_total_basic_salary];
        foreach ($allowances as $allowance) {
            $grandTotalRow[] = $allowanceTotals[$allowance->id];
        }

        $grandTotalRow[] = $grand_total_earn;

        foreach ($deductions as $deduction) {
            $grandTotalRow[] = $deductionTotals[$deduction->id];
        }

        $grandTotalRow[] = $grand_total_pf;
        $grandTotalRow[] = $grand_total_bank_loan;
        $grandTotalRow[] = $grand_total_stamp_duty;
        $grandTotalRow[] = $grand_total_deductions;
        $grandTotalRow[] = $grand_total_net_salary;
        $grandTotalRow[] = $grand_total_corporation_share;


        $data[] = $grandTotalRow;

        return collect($data);
    }



    public function headings(): array
    {
        $headings = [
            'Sr No.',
            'Employee Code',
            'Total Present Days',
            'Name',
            'Pay Scale',
            'Date of Appointment',
            'Designation',
            'Basic + GP',
        ];

        // Add allowance headings dynamically
        $allowances = Allowance::get();
        foreach ($allowances->chunk(5) as $chunk) {
            foreach ($chunk as $allowance) {
                $headings[] = substr($allowance->allowance, 0, 5);
            }
        }

        $headings[] = 'Total_Earn';

        $deductions = Deduction::get();
        foreach ($deductions->chunk(5) as $chunk) {
            foreach ($chunk as $deduction) {
                $headings[] = substr($deduction->deduction, 0, 5);
            }
        }

        $headings[] = 'PF Loan';
        $headings[] = 'Bank Loan';
        $headings[] = 'Stamp Duty';
        $headings[] = 'Total Deduct';

        $headings[] = 'Net Salary';
        $headings[] = 'Corporation Share';
        $headings[] = 'Remark';

        return $headings;
    }
}