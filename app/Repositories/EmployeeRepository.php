<?php

namespace App\Repositories;

use App\Models\Employee;
use App\Models\EmployeeLeaves;
use App\Models\AcademicDetails;
use App\Models\Experience;
use App\Models\LeaveType;
use App\Models\Ward;
use App\Models\Designation;
use App\Models\Department;
use App\Models\Clas;
use App\Models\Bank;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EmployeeRepository
{

    public function store($input)
    {
        DB::beginTransaction();

        $employee = Employee::create(Arr::only($input, Employee::getFillables()));

        // Handle Employee Leaves
        if ($input['leave_type_id'][0] != null) {
            foreach ($input['leave_type_id'] as $key => $leave_type_id) {
                $LeaveData = [
                    'employee_id' => $employee->id,
                    'leave_type_id' => $leave_type_id,
                    'carry_forward' => $input['carry_forward'][$key],
                    'no_of_leaves' => $input['no_of_leaves'][$key],
                    'encashable' => $input['encashable'][$key],
                ];

                EmployeeLeaves::create($LeaveData);
            }
        }

        // Handle AcademicDetails
        if ($input['qfrom'][0] != null) {
            foreach ($input['qfrom'] as $key => $qfrom) {
                // Check if document file is provided
                if (request()->hasFile('qdocument') && isset(request()->file('qdocument')[$key])) {
                    $documentFile = request()->file('qdocument')[$key];

                    // Validate and store the file
                    $documentFilePath = $documentFile->store('academic_documents', 'public');
                } else {
                    // Handle case where document file is not provided
                    $documentFilePath = null;
                }

                $educationData = [
                    'employee_id' => $employee->id,
                    'qfrom' => $qfrom,
                    'qto' => $input['qto'][$key],
                    'qcertificate' => $input['qcertificate'][$key],
                    'qboard' => $input['qboard'][$key],
                    'qmarks' => $input['qmarks'][$key],
                    'qdocument' => $documentFilePath,
                ];

                AcademicDetails::create($educationData);
            }
        }

        // Handle Experience
        if ($input['efrom'][0] != null) {
            foreach ($input['efrom'] as $key => $efrom) {
                $experienceData = [
                    'employee_id' => $employee->id,
                    'efrom' => $efrom,
                    'eto' => $input['eto'][$key],
                    'ename_address' => $input['ename_address'][$key],
                    'edesignation' => $input['edesignation'][$key],
                    //  'created_by' => Auth::user()->id, // Adjust as needed
                ];

                Experience::create($experienceData);
            }
        }

        // Commit the transaction
        DB::commit();
    }

    public function editEmployee($employee)
    {
        if ($employee) {

            $authUser = Auth::user();
            $wards = Ward::when(
                $authUser->hasRole(['Ward HOD', 'Department HOD']),
                function ($query) use ($authUser) {
                    return $query->where('id', $authUser->ward_id);
                }
            )->latest()->get();

            $departments = Department::where('ward_id', $employee->ward_id)->latest()->get();
            $class = Clas::latest()->get();
            $designations = Designation::where('ward_id', $employee->ward_id)
                ->where('department_id', $employee->department_id)
                ->where('clas_id', $employee->clas_id)
                ->latest()->get();

            $banks = Bank::latest()->get();

            $Uniqueid = $employee->id;

            $academicDetails = AcademicDetails::where('employee_id', $Uniqueid)->latest()->get();
            $experience = Experience::where('employee_id', $Uniqueid)->latest()->get();

            $leaveTypes = LeaveType::with(['employeeLeaves' => function ($query) use ($Uniqueid) {
                $query->where('employee_id', $Uniqueid);
            }])->latest()->get();

            $leaveType = $leaveTypes->map(function ($type) {
                return [
                    'employee_leaves_id'    => $type->id,
                    'type'                  => $type->name,
                    'leave_type_id'         => $type->id,
                    'carry_forward'         => $type->carry_forward,
                    'no_of_leaves'          => optional($type->employeeLeaves)->no_of_leaves ?: $type->no_of_leaves,
                    'encashable'            => optional($type->employeeLeaves)->encashable ?: $type->encashable,
                ];
            });

            return [
                'employee'        => $employee,
                'wards'           => $wards,
                'departments'     => $departments,
                'designations'    => $designations,
                'class'           => $class,
                'leaveType'       => $leaveType,
                'banks'           => $banks,
                'academicDetails' => $academicDetails,
                'experience'      => $experience,
            ];
        }
    }

    public function updateEmployee($input, $emp)
    {

        DB::beginTransaction();

        $employee = $emp->update(Arr::only($input, Employee::getFillables()));

        // Handle Employee Leaves

        if ($input['leave_type_id'][0] != null) {
            foreach ($input['leave_type_id'] as $key => $leave_type_id) {
                $LeaveData = [
                    'employee_id' => $emp->id,
                    'leave_type_id' => $leave_type_id,
                    'carry_forward' => $input['carry_forward'][$key],
                    'no_of_leaves' => $input['no_of_leaves'][$key],
                    'encashable' => $input['encashable'][$key],
                ];

                $employeeLeavesId = $input['employee_leaves_id'][$key];

                $existingLeave = EmployeeLeaves::find($employeeLeavesId);
                if ($existingLeave) {
                    $existingLeave->update($LeaveData);
                } else {
                    EmployeeLeaves::create($LeaveData);
                }
            }
        }


        // Handle AcademicDetails
        if ($input['qfrom'][0] != null && !empty($input['qfrom'])) {
            foreach ($input['qfrom'] as $key => $qfrom) {
                // Check if document file is provided
                if (request()->hasFile('qdocument') && isset(request()->file('qdocument')[$key])) {
                    $documentFile = request()->file('qdocument')[$key];

                    // Validate and store the file
                    $documentFilePath = $documentFile->store('academic_documents', 'public');
                } else {
                    // Handle case where document file is not provided
                    $documentFilePath = $input['existingqdocument'][$key] ?? null;
                }

                $educationData = [
                    'employee_id' => $emp->id,
                    'qfrom' => $qfrom,
                    'qto' => $input['qto'][$key],
                    'qcertificate' => $input['qcertificate'][$key],
                    'qboard' => $input['qboard'][$key],
                    'qmarks' => $input['qmarks'][$key],
                    'qdocument' => $documentFilePath,
                ];

                if (isset($input['academicDetailsId'][$key])) {
                    AcademicDetails::find($input['academicDetailsId'][$key])->update($educationData);
                } else {
                    AcademicDetails::create($educationData);
                }
            }

            if (isset($input['deletedAcademicIds'])) {
                AcademicDetails::destroy($input['deletedAcademicIds']);
            }
        }

        //    // Handle Experience
        if ($input['efrom'][0] != null) {
            foreach ($input['efrom'] as $key => $efrom) {
                $experienceData = [
                    'employee_id' => $emp->id,
                    'efrom' => $efrom,
                    'eto' => $input['eto'][$key],
                    'ename_address' => $input['ename_address'][$key],
                    'edesignation' => $input['edesignation'][$key],
                ];

                if (isset($input['experienceId'][$key])) {
                    Experience::find($input['experienceId'][$key])->update($experienceData);
                } else {
                    Experience::create($experienceData);
                }
            }

            if (isset($input['deletedExperienceIds'])) {
                Experience::destroy($input['deletedExperienceIds']);
            }
        }

        // Commit the transaction
        DB::commit();
    }
}