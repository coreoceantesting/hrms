<?php

namespace App\Http\Controllers\Admin\Masters;

use App\Http\Controllers\Admin\Controller;

use App\Http\Requests\Admin\Masters\StoreDeductionRequest;
use App\Http\Requests\Admin\Masters\UpdateDeductionRequest;
use Illuminate\Http\Request;
use App\Models\Deduction;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DeductionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $deduction = Deduction::latest()->get();

        return view('admin.masters.deduction')->with(['deductions' => $deduction]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDeductionRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            Deduction::create(Arr::only($input, Deduction::getFillables()));
            DB::commit();

            return response()->json(['success' => 'Deduction created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Deduction');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Deduction $deduction)
    {
        if ($deduction) {

            $typeHtml = '<span>
            <option value="">--Select Type--</option>';
            $typeHtml .= '<option value="1" ' . ($deduction->type == '1' ? 'selected' : '') . '>Amount</option>';
            $typeHtml .= '<option value="2" ' . ($deduction->type == '2' ? 'selected' : '') . '>Percentage</option>';
            $typeHtml .= '</span>';

            $calculationHtml = '<span>
            <option value="">--Select Calculation--</option>';
            $calculationHtml .= '<option value="1" ' . ($deduction->calculation == '1' ? 'selected' : '') . '>Fixed</option>';
            $calculationHtml .= '<option value="2" ' . ($deduction->calculation == '2' ? 'selected' : '') . '>Dynamic</option>';
            $calculationHtml .= '</span>';

            $response = [
                'result' => 1,
                'deduction' => $deduction,
                'typeHtml' => $typeHtml,
                'calculationHtml'   => $calculationHtml,
            ];
        } else {
            $response = ['result' => 0];
        }
        return $response;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDeductionRequest $request, Deduction $deduction)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();
            $deduction->update(Arr::only($input, Deduction::getFillables()));
            DB::commit();
            return response()->json(['success' => 'Deduction updated successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'updating', 'Deduction');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Deduction $deduction)
    {
        try {
            DB::beginTransaction();
            $deduction->delete();
            DB::commit();
            return response()->json(['success' => 'Deduction deleted successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'deleting', 'Deduction');
        }
    }
}
