<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Report\PaySheetExcelRequest;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Department;
use App\Models\FreezeAttendance;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Exports\PaySheetExport;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PaySheetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $authUser = Auth::user();
        $departments = Department::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
            return $query->where('ward_id', $authUser->ward_id);
        })
            ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                return $query->where('id', $authUser->department_id);
            })->latest()->get();

        return view('admin.reports.pay-sheet')->with(['departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PaySheetExcelRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->validated();

            return Excel::download(new PaySheetExport, 'pay_sheet.xlsx');

            // Allowance::create(Arr::only($input, Allowance::getFillables()));
            // DB::commit();

            return response()->json(['success' => 'Excel Export created successfully!']);
        } catch (\Exception $e) {
            return $this->respondWithAjax($e, 'creating', 'Excel Export');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function showPaySheetPDF(PaySheetExcelRequest $request)
    {
        $authUser = Auth::user();
        $department = $request->department;
        $month = $request->month;
        $data['from_date'] = $request->from_date;
        $data['to_date'] = $request->to_date;

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $data['allowances'] = Allowance::get();
        $data['deductions'] = Deduction::get();

        $data['freezeAttendances'] = FreezeAttendance::with('employee', 'designation')->where('department_id', $department)
            ->where('from_date', $data['from_date'])
            ->where('to_date', $data['to_date'])
            ->where('month', $month)
            ->where('freeze_status', 1)
            // ->where('employee_id', 1)
            ->get();

        $department_name = Department::where('id', $department)->first();

        $filename = "Pay Sheet " . $department_name->name . " Report" . '.pdf';
        $pdf = SnappyPdf::loadView('admin.reports.pdf.pay-sheet-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('landscape')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }
}
