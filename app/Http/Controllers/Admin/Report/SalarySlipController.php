<?php

namespace App\Http\Controllers\Admin\Report;

use App\Http\Controllers\Controller;
use App\Models\Allowance;
use App\Models\Corporation;
use App\Models\Deduction;
use App\Models\Employee;
use App\Models\FinancialYear;
use App\Models\FreezeAttendance;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;
use Illuminate\Http\Response;

class SalarySlipController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try {
            $authUser = Auth::user();
            $from_date = $request->from_date ?? null;
            $to_date = $request->to_date ?? null;
            $month = $request->month ?? date('m');

            // If from date and to date are not provided, calculate current month's from date and to date
            $financial_year = FinancialYear::where('id', session('financial_year'))->first();
            if (!$from_date || !$to_date) {
                if ($financial_year) {
                    if ($month <= 3) {
                        $year = date('Y', strtotime($financial_year->to_date));
                    } else {
                        $year = date('Y', strtotime($financial_year->from_date));
                    }
                    $month = $month ?? 1;

                    // $from_date = Carbon::parse($year . '-' . ($month) . '-' . 16)->subMonth()->toDateString();
                    $from_date = Carbon::parse($year . '-' . ($month) . '-' . 16);
                    $to_date = clone ($from_date);
                    $from_date = (string) $from_date->subMonth()->toDateString();
                    $to_date = (string) $to_date->subDay()->toDateString();
                }
            }

            $salary_slips = FreezeAttendance::when($authUser->hasRole(['Ward HOD']), function ($query) use ($authUser) {
                return $query->where('ward_id', $authUser->ward_id);
            })
                ->when($authUser->hasRole(['Department HOD']), function ($query) use ($authUser) {
                    return $query->where('department_id', $authUser->department_id);
                })
                ->where('freeze_status', 1)
                ->where('from_date', $from_date)
                ->where('to_date', $to_date)
                ->get();

            return view('admin.reports.salary-slips')->with(['salary_slips' => $salary_slips, 'from_date' => $from_date, 'to_date' => $to_date, 'month' => $month]);
        } catch (\Exception $e) {
            return response()->json([
                'error2' => 'An error occurred while processing the request.'
            ], 500);
        }
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $data['freezeAttendance'] = FreezeAttendance::where('id', $id)->first();;
        $data['employee_details'] = Employee::with('ward', 'department', 'designation')->where('id', $data['freezeAttendance']->employee_id)->first();
        $data['allowances'] = Allowance::latest()->get();
        $data['deductions'] = Deduction::latest()->get();

        $data['explode_allowance_ids'] = explode(',', $data['freezeAttendance']->allowance_Id);
        $data['explode_allowance_type'] = explode(',', $data['freezeAttendance']->allowance_Type);
        $data['explode_allowance_amt'] = explode(',', $data['freezeAttendance']->allowance_Amt);


        $data['explode_deduction_ids'] = explode(',', $data['freezeAttendance']->deduction_Id);
        $data['explode_deduction_amt'] = explode(',', $data['freezeAttendance']->deduction_Amt);
        $data['explode_deduction_type'] = explode(',', $data['freezeAttendance']->deduction_Type);

        $data['corporation'] = Corporation::first();

        $logoPath = public_path($data['corporation']->logo);

        if (file_exists($logoPath)) {
            $logoData = file_get_contents($logoPath);
            $base64Logo = base64_encode($logoData);
        } else {
            // Handle if the logo file doesn't exist
            $base64Logo = null;
        }

        $data['base64Logo'] = $base64Logo;

        $filename = "Salary Slip" . " " . $data['freezeAttendance']->Emp_Code . '.pdf';
        $pdf = SnappyPdf::loadView('admin.reports.pdf.salary-slip-pdf', $data)
            ->setPaper('a4')
            ->setOrientation('portrait')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-top', 3)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0);

        return $pdf->inline($filename);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
