<?php

namespace App\Http\Requests\Admin\Masters;

use Illuminate\Foundation\Http\FormRequest;

class StorePayScaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'pay_mst_id' => 'required',
            'level' => 'required',
            'grade_amp' => 'required',
            'grade_pay_name' => 'required',
            'pay_band' => 'required',
            'pay_band_scale' => 'required',
            'amount' => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'pay_mst_id.required' => 'The select Pay field is required',
        ];
    }
}
