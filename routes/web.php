<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
})->name('/');




// Guest Users
Route::middleware(['guest', 'PreventBackHistory'])->group(function () {
    Route::get('login', [App\Http\Controllers\Admin\AuthController::class, 'showLogin'])->name('login');
    Route::post('login', [App\Http\Controllers\Admin\AuthController::class, 'login'])->name('signin');
    Route::get('register', [App\Http\Controllers\Admin\AuthController::class, 'showRegister'])->name('register');
    Route::post('register', [App\Http\Controllers\Admin\AuthController::class, 'register'])->name('signup');
});




// Authenticated users
Route::middleware(['auth', 'PreventBackHistory'])->group(function () {

    // Auth Routes
    Route::get('home', fn () => redirect()->route('dashboard'))->name('home');
    Route::get('dashboard', [App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
    Route::post('logout', [App\Http\Controllers\Admin\AuthController::class, 'Logout'])->name('logout');
    Route::get('change-theme-mode', [App\Http\Controllers\Admin\DashboardController::class, 'changeThemeMode'])->name('change-theme-mode');
    Route::get('show-change-password', [App\Http\Controllers\Admin\AuthController::class, 'showChangePassword'])->name('show-change-password');
    Route::post('change-password', [App\Http\Controllers\Admin\AuthController::class, 'changePassword'])->name('change-password');



    // Masters
    Route::resource('wards', App\Http\Controllers\Admin\Masters\WardController::class);
    Route::resource('department', App\Http\Controllers\Admin\Masters\DepartmentController::class);
    Route::resource('class', App\Http\Controllers\Admin\Masters\ClassController::class);
    Route::resource('bank', App\Http\Controllers\Admin\Masters\BankController::class);
    Route::resource('financial_year', App\Http\Controllers\Admin\Masters\FinancialYearController::class);
    Route::resource('allowance', App\Http\Controllers\Admin\Masters\AllowanceController::class);
    Route::resource('deduction', App\Http\Controllers\Admin\Masters\DeductionController::class);
    Route::resource('loan', App\Http\Controllers\Admin\Masters\LoanController::class);
    // To Change Activity Status of Loan
    Route::post('loan-activity-status/{model_id}/{btn_status}', [App\Http\Controllers\Admin\Masters\LoanController::class, 'changeStatus'])->name('loan-activity-status');

    Route::resource('designation', App\Http\Controllers\Admin\Masters\DesignationController::class);
    // Fetch departments on ward
    Route::get('fetch-departments/{ward_id}', [App\Http\Controllers\Admin\Masters\DesignationController::class, 'fetchDepartment'])->name('fetch-departments');

    Route::resource('leave-type', App\Http\Controllers\Admin\Masters\LeaveTypeController::class);
    Route::resource('pay-scale', App\Http\Controllers\Admin\Masters\PayScaleController::class);
    Route::resource('status', App\Http\Controllers\Admin\Masters\StatusController::class);



    //Employee List
    Route::resource('employee', App\Http\Controllers\Admin\Employee\EmployeeController::class);
    // Fetch designation on ward/class/department
    Route::get('fetch-designations/{ward_id}/{department_id}/{class_id}', [App\Http\Controllers\Admin\Employee\EmployeeController::class, 'fetchDesignation'])->name('fetch-designations');
    Route::get('fetch-working-years/{class_id}/{dob}', [App\Http\Controllers\Admin\Employee\EmployeeController::class, 'fetchWorkingYear'])->name('fetch-working-years');

    Route::resource('employee-status', App\Http\Controllers\Admin\Employee\EmployeeStatusController::class);
    Route::get('fetch-employee-details/{Emp_Code}', [App\Http\Controllers\Admin\Employee\EmployeeStatusController::class, 'fetchEmployeeDetails'])
        ->name('fetch-employee-details');


    // Salary Structure
    Route::resource('employee-salary', App\Http\Controllers\Admin\Employee\EmployeeSalaryController::class);
    Route::get('fetch-payscale-details/{pay_scale_id}', [App\Http\Controllers\Admin\Employee\EmployeeSalaryController::class, 'fetchpayScaleDetails'])
        ->name('fetch-payscale-details');

    Route::resource('basic-salary-increment', App\Http\Controllers\Admin\Employee\BasicSalaryIncrementController::class);
    Route::get('get-incremented-salary', [App\Http\Controllers\Admin\Employee\BasicSalaryIncrementController::class, 'getIncrementedBasicSalary'])
        ->name('get-incremented-salary');

    Route::resource('allowance-increment', App\Http\Controllers\Admin\Employee\AllowanceIncrementController::class);
    Route::get('fetch-allowance-details/{allowance_id}', [App\Http\Controllers\Admin\Employee\AllowanceIncrementController::class, 'fetchAllowanceDetails'])
        ->name('fetch-allowance-details');
    Route::get('get-incremented-allowance', [App\Http\Controllers\Admin\Employee\AllowanceIncrementController::class, 'getIncrementedAllowance'])
        ->name('get-incremented-allowance');

    Route::resource('deduction-increment', App\Http\Controllers\Admin\Employee\DeductionIncrementController::class);
    Route::get('fetch-deduction-details/{deduction_id}', [App\Http\Controllers\Admin\Employee\DeductionIncrementController::class, 'fetchDeductionDetails'])
        ->name('fetch-deduction-details');
    Route::get('get-incremented-deduction', [App\Http\Controllers\Admin\Employee\DeductionIncrementController::class, 'getIncrementedDeduction'])
        ->name('get-incremented-deduction');
    // End Salary Structure

    // Employee Loans Start
    Route::resource('employee-loans', App\Http\Controllers\Admin\Employee\EmployeeLoanController::class);
    Route::post('employee-loan-status/{model_id}/{btn_status}', [App\Http\Controllers\Admin\Employee\EmployeeLoanController::class, 'employeeLoanStatus'])->name('employee-loan-status');

    Route::resource('employee-monthly-loans', App\Http\Controllers\Admin\Employee\EmployeeMonthlyLoanController::class);
    Route::get('employee-monthly-loans/{from_date}/{to_date}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyLoanController::class, 'showEmployeeLoans'])->name('employee-monthly-loans.showEmployeeLoans');
    Route::post('employee-monthly-loan-status/{model_id}', [App\Http\Controllers\Admin\Employee\EmployeeMonthlyLoanController::class, 'employeeMonthlyLoanStatus'])->name('employee-monthly-loan-status');

    // Employee Loans End

    // Freeze Attendance
    Route::resource('freeze-attendance', App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class);
    Route::get('employee-monthly-salary/{from_date}/{to_date}', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'showEmployeeSalary'])->name('employee-monthly-salary.showEmployeeSalary');
    Route::get('unfreeze-employee', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'unfreezeEmployee'])->name('unfreeze-employee');
    Route::get('freeze-employee', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'freezeEmployee'])->name('freeze-employee');

    Route::get('fetch-date-range/{month}', [App\Http\Controllers\Admin\Employee\Freeze\FreezeController::class, 'fetchDateRange'])
        ->name('fetch-date-range');
    // Freeze Attendance End


    // Reports Start
    Route::resource('salary-slips', App\Http\Controllers\Admin\Report\SalarySlipController::class);
    Route::resource('bank-statement', App\Http\Controllers\Admin\Report\BankStatementController::class);
    Route::resource('pay-sheet', App\Http\Controllers\Admin\Report\PaySheetController::class);
    Route::get('pay-sheet-pdf', [App\Http\Controllers\Admin\Report\PaySheetController::class, 'showPaySheetPDF'])->name('pay-sheet-pdf');

    Route::resource('bank-deduction-employee-report', App\Http\Controllers\Admin\Report\BankDeductionEmployeeController::class);
    Route::resource('bank-deduction-report', App\Http\Controllers\Admin\Report\BankDeductionController::class);
    Route::resource('summary-department-report', App\Http\Controllers\Admin\Report\GrandSummaryDepartmentController::class);
    Route::get('grand-summary-department-pdf', [App\Http\Controllers\Admin\Report\GrandSummaryDepartmentController::class, 'showGrandSummaryDepartmentPDF'])->name('grand-summary-department-pdf');
    Route::resource('summary-ward-report', App\Http\Controllers\Admin\Report\GrandSummaryWardController::class);
    Route::get('grand-summary-ward-pdf', [App\Http\Controllers\Admin\Report\GrandSummaryWardController::class, 'showGrandSummaryWardPDF'])->name('grand-summary-ward-pdf');
    Route::resource('summary-report', App\Http\Controllers\Admin\Report\GrandSummaryController::class);
    Route::get('grand-summary-pdf', [App\Http\Controllers\Admin\Report\GrandSummaryController::class, 'showGrandSummaryPDF'])->name('grand-summary-pdf');
    Route::resource('pay-sheet-excel', App\Http\Controllers\Admin\Report\PaySheetExcelController::class);

    Route::resource('allowance-report', App\Http\Controllers\Admin\Report\AllowanceReportController::class);
    Route::resource('deduction-report', App\Http\Controllers\Admin\Report\DeductionReportController::class);


    // Reports End

    // Users Roles n Permissions
    Route::resource('users', App\Http\Controllers\Admin\UserController::class);
    Route::get('users/{user}/toggle', [App\Http\Controllers\Admin\UserController::class, 'toggle'])->name('users.toggle');
    Route::get('users/{user}/retire', [App\Http\Controllers\Admin\UserController::class, 'retire'])->name('users.retire');
    Route::put('users/{user}/change-password', [App\Http\Controllers\Admin\UserController::class, 'changePassword'])->name('users.change-password');
    Route::get('users/{user}/get-role', [App\Http\Controllers\Admin\UserController::class, 'getRole'])->name('users.get-role');
    Route::put('users/{user}/assign-role', [App\Http\Controllers\Admin\UserController::class, 'assignRole'])->name('users.assign-role');
    Route::resource('roles', App\Http\Controllers\Admin\RoleController::class);
});




Route::get('/php', function (Request $request) {
    if (!auth()->check())
        return 'Unauthorized request';

    Artisan::call($request->artisan);
    return dd(Artisan::output());
});
