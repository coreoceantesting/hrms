@php
    use App\Models\Allowance;
    use App\Models\Deduction;
    use App\Models\EmployeeMonthlyLoan;

     $explode_loan_ids = explode(',', $freezeAttendance->loan_deduction_id);
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Salary Slip - {{ $freezeAttendance->emp_name }}</title>
    <style>


        body {
                font-family: "Source Sans 3", Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 16px;
            }

        @media print {
            body {
                -webkit-print-color-adjust: exact;
            }
        }

        /* body {
            font-family: Arial, sans-serif;
            font-size: 16px;
        } */
        .label {
            display: inline-block;
            width: 150px; /* Adjust width as needed */
        }
        .section {
            width: 100%;
            margin-left: 45px;
            margin-bottom: 20px;
        }
        .section-heading {
            margin: 10px 0 5px;
            font-size: 24px;
        }
        .subsection {
            width: 50%;
            float: left;
        }

        .subsection-details {
            width: 48%;
            float: left;
        }

        .table-container {
            width: 100%;
        }
        table {
            width: 100%;
            border-collapse: collapse; /* Collapse borders */
        }
        th, td {
            padding: 10px;
            font-size: 18px;
            border: 1px solid black; /* Single border */
            text-align: left;
            /* background-color: lightgray; */
        }
        .earnings-header, .deductions-header {
            background-color: #355495; /* Blue background color */
            color: #ffffff; /* White text color */
        }
        .dashed-hr {
            border: 1px dashed black;
            clear: both;
        }
        p {
            font-size: 16px;
        }
        .check
        {
            clear: both;
        }

    </style>
</head>
<body>

    <table style="width: 100%; border:none;">
        <thead>
            <tr>
                <td style="background-color:white; border:none; text-align:right">
                    @if(isset($base64Logo))
                    <img src="data:image/png;base64,{{ $base64Logo }}" alt="Corporation Logo" height="80" width="90">
                    @endif
                </td>
                <td style="background-color:white; border:none;">
                    <h2 class="section-heading" style="margin-left:10%;">{{ $corporation->name }}</h2>
                    <h5 class="section-heading" style="font-size: 18px; margin-left:18%;">Salary Slip for the month of {{ Carbon\Carbon::parse($freezeAttendance->to_date)->format('F Y') }}</h5>
                </td>
            </tr>
        </thead>
    </table>

    <div class="section">
        <div class="subsection-details" style=" border-right: 1px dashed black;">
            <p><span class="label">Ward</span><strong>: {{ $employee_details->ward->name }}</strong></p>
            <p><span class="label">Employee Name</span><strong>: {{ $freezeAttendance->emp_name }}</strong></p>
            <p><span class="label">Employee Code</span><strong>: {{ $freezeAttendance->Emp_Code }}</strong></p>
            {{-- <p><span class="label">Address</span></p> --}}
            <p><span class="label">PF A/C No</span><strong>: {{ $freezeAttendance->pf_account_no }}</strong></p>
            <p><span class="label">Payscale</span><strong>: {{ $freezeAttendance->pay_band_scale ." ".$freezeAttendance->grade_pay_scale }}</strong></p>
            <p><span class="label">Pan No</span><strong>: {{ $employee_details->pan }}</strong></p>
            <p><span class="label">Aadhar No.</span><strong>: {{ $employee_details->aadhar }}</strong></p>
        </div>
        <div class="subsection-details" style="margin-left: 2%;">
            <p><span class="label">Department</span><strong>: {{ $employee_details->department->name }}</strong></p>
            <p><span class="label">Designation</span><strong>: {{ $employee_details->designation->name }}</strong></p>
            <p><span class="label">Date of Appointment</span><strong>: {{ $freezeAttendance->date_of_appointment }}</strong></p>
            {{-- <p><span class="label">Date of Increment</span></p> --}}
            <p><span class="label">Date of Retirement</span><strong>: {{ $freezeAttendance->date_of_retirement }}</strong></p>
            <p><span class="label">Bank A/C No.</span><strong>: {{ $freezeAttendance->bank_account_number }}</strong></p>
            <p><span class="label">Phone No.</span><strong>: {{ $freezeAttendance->phone_no }}</strong></p>
            <p><span class="label">Actual Basic</span><strong>: {{ $freezeAttendance->actual_basic }}</strong></p>
        </div>
    </div>

    <hr class="dashed-hr" style="margin-bottom: 3%;">
    @if($freezeAttendance->basic_salary != 0)
    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="earnings-header">EARNINGS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>BASIC SALARY</td>
                            <td><b>{{ $freezeAttendance->basic_salary }}</b></td>
                        </tr>
                        {{-- Allowance --}}
                        @foreach($explode_allowance_ids as $key => $allowance_id)
                            @php $allowance = Allowance::find($allowance_id); @endphp
                            <tr>
                                <td>{{ $allowance?->allowance }}</td>
                                <td><b>{{ $explode_allowance_amt[$key] }}</b></td>
                            </tr>
                        @endforeach
                        @php

                            $deduction_rows =  count($explode_deduction_ids) + 1; //including stamp duty
                            if ($freezeAttendance->loan_deduction_id)
                                $deduction_rows += ($explode_loan_ids)? count($explode_loan_ids) : 0;
                        @endphp
                        @if($deduction_rows > count($explode_allowance_ids) + 1)
                        @php
                            $no_of_rows = $deduction_rows - (count($explode_allowance_ids) + 1); // including basic
                        @endphp
                        @for($i = 0; $i < $no_of_rows; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
        <div class="subsection" style="width: 45%;">
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                            <th colspan="2" class="deductions-header">DEDUCTIONS</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- Deductions --}}
                        @foreach($explode_deduction_ids as $key => $deduction_id)
                            @php $deduction = Deduction::find($deduction_id); @endphp
                            <tr>
                                <td>{{ $deduction?->deduction }}</td>
                                <td><b>{{ $explode_deduction_amt[$key] }}</b></td>
                            </tr>
                        @endforeach

                        {{-- Employee Loan --}}
                        @if ($freezeAttendance->loan_deduction_id)
                            @foreach ($explode_loan_ids as $loan_id)
                                @php $emp_loan = EmployeeMonthlyLoan::with('loan')->where('id', $loan_id)->first(); @endphp
                                <tr>
                                    <td>{{ $emp_loan?->loan?->loan . ' (' . $emp_loan?->installment_no.')' }}</td>
                                    <td><b>{{ $emp_loan?->installment_amount }}</b></td>
                                </tr>
                            @endforeach
                        @endif
                        <tr>
                            <td>STAMP DUTY</td>
                            <td><b>{{ $freezeAttendance->stamp_duty }}</b></td>
                        </tr>

                        @if(count($explode_allowance_ids) + 1 > $deduction_rows)
                        @php
                            $no_of_rows_deduction = count($explode_allowance_ids) + 1 - $deduction_rows;
                        @endphp
                        @for($i = 0; $i < $no_of_rows_deduction; $i++)
                        <tr>
                            <td><br></td>
                            <td><br></td>
                        </tr>
                        @endfor
                        @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <p class="check">

    <div class="section" style="margin-left:25px;">
        <div class="subsection">
            <table>
                <tr>
                    <td><b>TOTAL EARNINGS</b></td>
                    <td><b>{{ $freezeAttendance->basic_salary + $freezeAttendance->total_allowance }}</b></td>
                </tr>
            </table>
        </div>
        <div class="subsection" style="width: 45%;">
            <table>
                <tr>
                    <td><b>TOTAL DEDUCTIONS</b></td>
                    <td><b>{{ $freezeAttendance->total_deduction }}</b></td>
                </tr>
                <tr>
                    <td><b>NET SALARY</b></td>
                    <td ><b>{{ $freezeAttendance->net_salary }}</b></td>
                </tr>
            </table>
        </div>
    </div>
    @else
    <h3 style="margin-left:25px;">No Salary Generate For This Month</h3>
    @endif


    <hr class="dashed-hr" style="margin-top: 12%;">
    <div style="margin-left:25px;">
    <p><b>Present Days: {{ $freezeAttendance->present_day }}</b></p>
    <p><b>Remark:</b> </p>
    <p><b>This is a computer-generated payslip. It does not require any authorized sign.</b></p>
    <p><b>Date: {{ date('d-m-Y') }} <span style="margin-left:60%;">Establishment Clerk</span></b></p>
    <p><b>Generated & Download by: {{ Auth::user()->name }}</b></p>
    </div>

</body>
</html>
