<x-admin.layout>
    <x-slot name="title">Monthly Loan</x-slot>
    <x-slot name="heading">Monthly Loan</x-slot>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee_monthly_loans as $employee_monthly_loan)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $employee_monthly_loan->from_date }}</td>
                                        <td>{{ $employee_monthly_loan->to_date }}</td>
                                        <td>
                                            @can('employee-monthly-loans.view')
                                            <a href="{{ route('employee-monthly-loans.showEmployeeLoans', ['from_date' => $employee_monthly_loan->from_date, 'to_date' => $employee_monthly_loan->to_date]) }}">
                                                <button class="edit-element btn btn-secondary px-2 py-1" title="Show Loans" data-id="{{ $employee_monthly_loan->from_date }}">
                                                    <i data-feather="eye"></i>
                                                </button>
                                            </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-admin.layout>

