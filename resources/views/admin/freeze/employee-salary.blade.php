<x-admin.layout>
    <x-slot name="title">Employee Salary</x-slot>
    <x-slot name="heading">Employee Salary</x-slot>

    <!-- Add Form -->
    <div class="row" id="addContainer">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="">
                                <a href="{{ route('freeze-attendance.index') }}" id="btnCancel1" class="btn btn-danger">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card header -->
                <div class="card-body">
                        <div class="step-arrow-nav mb-4">

                            <ul class="nav nav-pills custom-nav nav-justified" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="gen-info-tab" data-bs-toggle="pill" data-bs-target="#gen-info" type="button" role="tab">Freeze</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="leave-info-tab" data-bs-toggle="pill" data-bs-target="#leave-info" type="button" role="tab">Unfreeze</button>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="gen-info" role="tabpanel">
                                <div class="table-responsive">
                                    <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Employee Id</th>
                                                <th>Employee Name</th>
                                                <th>Net Salary</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employee_monthly_salaries as $employee_monthly_salary)
                                            @if($employee_monthly_salary->freeze_status == 1)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $employee_monthly_salary->from_date }}</td>
                                                    <td>{{ $employee_monthly_salary->to_date }}</td>
                                                    <td>{{ $employee_monthly_salary->Emp_Code }}</td>
                                                    <td>{{ $employee_monthly_salary->emp_name }}</td>
                                                    <td>{{ $employee_monthly_salary?->net_salary }}</td>
                                                    <td>
                                                        @can('freeze-attendance.edit')
                                                        <button id="unfreeze-btn" class="edit-element btn btn-secondary px-2 py-1 unfreeze-btn" title="Show Loans" data-id="{{ $employee_monthly_salary->id }}">
                                                            Unfreeze
                                                        </button>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endif
                                            @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- end tab pane -->

                            <div class="tab-pane fade" id="leave-info" role="tabpanel">
                                <div class="table-responsive">
                                    <table id="buttons-datatables" class="table table-bordered nowrap align-middle" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Employee Id</th>
                                                <th>Employee Name</th>
                                                <th>Net Salary</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($employee_monthly_salaries as $employee_monthly_salary)
                                            @if($employee_monthly_salary->freeze_status == 0)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $employee_monthly_salary->from_date }}</td>
                                                    <td>{{ $employee_monthly_salary->to_date }}</td>
                                                    <td>{{ $employee_monthly_salary->Emp_Code }}</td>
                                                    <td>{{ $employee_monthly_salary->emp_name }}</td>
                                                    <td>{{ $employee_monthly_salary?->net_salary }}</td>
                                                    <td>
                                                        @can('freeze-attendance.edit')
                                                            <button class="edit-element btn btn-secondary px-2 py-1 freeze-btn" title="Show" data-id="{{ $employee_monthly_salary->id }}">
                                                                Freeze
                                                            </button>
                                                        @endcan
                                                    </td>
                                                </tr>
                                            @endif
                                            @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- end tab pane -->


                        </div>
                        <!-- end tab content -->
                </div>
                <!-- end card body -->
            </div>
            <!-- end card -->
        </div>
    </div>

</x-admin.layout>

<!-- unfreeze -->
<script>
    $(".unfreeze-btn").on("click", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to unfreeze?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('unfreeze-employee') }}";

                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            '_method': "GET",
                            'id':model_id
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>

<script>
        $(".freeze-btn").on("click", function(e) {
        e.preventDefault();
        swal({
                title: "Are you sure to freeze?",
                icon: "info",
                buttons: ["Cancel", "Confirm"]
            })
            .then((justTransfer) => {
                if (justTransfer) {
                    var model_id = $(this).attr("data-id");
                    var url = "{{ route('freeze-employee') }}";

                    $.ajax({
                        url: url,
                        type: 'GET',
                        data: {
                            '_method': "GET",
                            'id':model_id
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (!data.error && !data.error2) {
                                swal("Success!", data.success, "success")
                                    .then((action) => {
                                        window.location.reload();
                                    });
                            } else {
                                if (data.error) {
                                    swal("Error!", data.error, "error");
                                } else {
                                    swal("Error!", data.error2, "error");
                                }
                            }
                        },
                        error: function(error, jqXHR, textStatus, errorThrown) {
                            swal("Error!", "Something went wrong", "error");
                        },
                    });
                }
            });
    });
</script>
