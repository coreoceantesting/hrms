<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="index.html" class="logo logo-dark">
            <span class="logo-sm">
                <img src="{{ asset('admin/images/logo-sm.png') }}" alt="" height="22" />
            </span>
            <span class="logo-lg">
                <img src="{{ asset('admin/images/logo-dark.png') }}" alt="" height="17" />
            </span>
        </a>
        <!-- Light Logo-->
        <a href="index.html" class="logo logo-light">
            <span class="logo-sm">
                <img src="{{ asset('admin/images/logo-sm.png') }}" alt="" height="22" />
            </span>
            <span class="logo-lg">
                <img src="{{ asset('admin/images/logo-light.png') }}" alt="" height="17" />
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <div id="two-column-menu"></div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title">
                    <span data-key="t-menu">Menu</span>
                </li>

                <li class="nav-item">
                    <a class="nav-link menu-link" href="{{ route('dashboard') }}">
                        <i class="ri-dashboard-2-line"></i>
                        <span data-key="t-dashboards">Dashboard</span>
                    </a>
                </li>


                @canany(['wards.view', 'departments.view', 'class.view', 'bank.view', 'financial_year.view', 'allowance.view', 'deduction.view', 'loan.view', 'designation.view', 'leaveType.view', 'pay_scale.view'])
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-3-line"></i>
                            <span data-key="t-layouts">Masters</span>
                        </a>
                        <div class="collapse menu-dropdown" id="sidebarLayouts">
                            <ul class="nav nav-sm flex-column">
                                @can('wards.view')
                                    <li class="nav-item">
                                        <a href="{{ route('wards.index') }}" class="nav-link" data-key="t-horizontal">Wards</a>
                                    </li>
                                @endcan
                                @can('departments.view')
                                    <li class="nav-item">
                                        <a href="{{ route('department.index') }}" class="nav-link" data-key="t-horizontal">Department</a>
                                    </li>
                                @endcan
                                @can('class.view')
                                    <li class="nav-item">
                                        <a href="{{ route('class.index') }}" class="nav-link" data-key="t-horizontal">Class</a>
                                    </li>
                                @endcan
                                @can('bank.view')
                                    <li class="nav-item">
                                        <a href="{{ route('bank.index') }}" class="nav-link" data-key="t-horizontal">Bank</a>
                                    </li>
                                @endcan
                                @can('financial_year.view')
                                    <li class="nav-item">
                                        <a href="{{ route('financial_year.index') }}" class="nav-link" data-key="t-horizontal">Financial Year</a>
                                    </li>
                                @endcan
                                @can('allowance.view')
                                    <li class="nav-item">
                                        <a href="{{ route('allowance.index') }}" class="nav-link" data-key="t-horizontal">Allowance</a>
                                    </li>
                                @endcan
                                @can('deduction.view')
                                    <li class="nav-item">
                                        <a href="{{ route('deduction.index') }}" class="nav-link" data-key="t-horizontal">Deduction</a>
                                    </li>
                                @endcan
                                @can('loan.view')
                                    <li class="nav-item">
                                        <a href="{{ route('loan.index') }}" class="nav-link" data-key="t-horizontal">Loan</a>
                                    </li>
                                @endcan
                                @can('designation.view')
                                    <li class="nav-item">
                                        <a href="{{ route('designation.index') }}" class="nav-link" data-key="t-horizontal">Designation</a>
                                    </li>
                                @endcan
                                @can('leaveType.view')
                                    <li class="nav-item">
                                        <a href="{{ route('leave-type.index') }}" class="nav-link" data-key="t-horizontal">Leave Type</a>
                                    </li>
                                @endcan
                                @can('pay_scale.view')
                                <li class="nav-item">
                                    <a href="{{ route('pay-scale.index') }}" class="nav-link" data-key="t-horizontal">Pay Scale</a>
                                </li>
                                @endcan
                                @can('status.view')
                                <li class="nav-item">
                                    <a href="{{ route('status.index') }}" class="nav-link" data-key="t-horizontal">Status</a>
                                </li>
                                @endcan
                            </ul>
                        </div>
                    </li>
                @endcan


                @canany(['users.view', 'roles.view'])
                    <li class="nav-item">
                        <a class="nav-link menu-link" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                            <i class="ri-layout-3-line"></i>
                            <span data-key="t-layouts">User Management</span>
                        </a>
                        <div class="collapse menu-dropdown" id="sidebarLayouts">
                            <ul class="nav nav-sm flex-column">
                                @can('users.view')
                                    <li class="nav-item">
                                        <a href="{{ route('users.index') }}" class="nav-link" data-key="t-horizontal">Users</a>
                                    </li>
                                @endcan
                                @can('roles.view')
                                    <li class="nav-item">
                                        <a href="{{ route('roles.index') }}" class="nav-link" data-key="t-horizontal">Roles</a>
                                    </li>
                                @endcan
                            </ul>
                        </div>
                    </li>
                @endcan

                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Employee</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('employee.view')
                                <li class="nav-item">
                                    <a href="{{ route('employee.index') }}" class="nav-link" data-key="t-horizontal">List</a>
                                </li>
                            @endcan
                            @can('employee.create')
                                <li class="nav-item">
                                    <a href="{{ route('employee.create') }}" class="nav-link" data-key="t-horizontal">Add</a>
                                </li>
                            @endcan
                            @can('employee_status.create')
                            <li class="nav-item">
                                <a href="{{ route('employee-status.index') }}" class="nav-link" data-key="t-horizontal">Status</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>

                @canany(['employee-salary.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarAuth" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarAuth">
                        <i class="ri-account-circle-line"></i> <span data-key="t-authentication">Salary</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarAuth">
                        <ul class="nav nav-sm flex-column">
                            @can('employee-salary.view')
                                <li class="nav-item">
                                    <a href="{{ route('employee-salary.index') }}" class="nav-link" data-key="t-horizontal">Salary Structure</a>
                                </li>
                            @endcan
                            @canany(['basic-salary-increment.view','allowance-increment.view','deduction-increment.view'])
                            <li class="nav-item">
                                <a href="#sidebarSignUp" class="nav-link" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSignUp" data-key="t-signup"> Salary Increment
                                </a>
                                <div class="collapse menu-dropdown" id="sidebarSignUp">
                                    <ul class="nav nav-sm flex-column">
                                        @can('basic-salary-increment.view')
                                        <li class="nav-item">
                                            <a href="{{ route('basic-salary-increment.index') }}" class="nav-link" data-key="t-basic"> Basic
                                            </a>
                                        </li>
                                        @endcan
                                        @can('allowance-increment.view')
                                        <li class="nav-item">
                                            <a href="{{ route('allowance-increment.index') }}" class="nav-link" data-key="t-cover"> Allowance
                                            </a>
                                        </li>
                                        @endcan
                                        @can('deduction-increment.view')
                                        <li class="nav-item">
                                            <a href="{{ route('deduction-increment.index') }}" class="nav-link" data-key="t-cover"> Deduction
                                            </a>
                                        </li>
                                        @endcan
                                    </ul>
                                </div>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan

                @canany(['employee-loans.view','employee-monthly-loans.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Employee Loan</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('employee-loans.view')
                                <li class="nav-item">
                                    <a href="{{ route('employee-loans.index') }}" class="nav-link" data-key="t-horizontal">Loans</a>
                                </li>
                            @endcan

                            @can('employee-monthly-loans.view')
                            <li class="nav-item">
                                <a href="{{ route('employee-monthly-loans.index') }}" class="nav-link" data-key="t-horizontal">Monthly Loans</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan

                @canany(['freeze-attendance.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Freeze Attendance</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('freeze-attendance.view')
                                <li class="nav-item">
                                    <a href="{{ route('freeze-attendance.index') }}" class="nav-link" data-key="t-horizontal">Freeze</a>
                                </li>
                            @endcan

                        </ul>
                    </div>
                </li>
                @endcan

                @canany(['salary-slips.view','bank-statement.view','pay-sheet-pdf.view','bank-deduction-employee-report.view','pay-sheet-excel.view'])
                <li class="nav-item">
                    <a class="nav-link menu-link" href="#sidebarLayouts" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarLayouts">
                        <i class="ri-layout-3-line"></i>
                        <span data-key="t-layouts">Reports</span>
                    </a>
                    <div class="collapse menu-dropdown" id="sidebarLayouts">
                        <ul class="nav nav-sm flex-column">
                            @can('salary-slips.view')
                                <li class="nav-item">
                                    <a href="{{ route('salary-slips.index') }}" class="nav-link" data-key="t-horizontal">Salary Slip</a>
                                </li>
                            @endcan
                            @can('bank-statement.view')
                                <li class="nav-item">
                                    <a href="{{ route('bank-statement.index') }}" class="nav-link" data-key="t-horizontal">Bank Statement</a>
                                </li>
                            @endcan
                            @can('pay-sheet-pdf.view')
                                <li class="nav-item">
                                    <a href="{{ route('pay-sheet.index') }}" class="nav-link" data-key="t-horizontal">Pay Sheet</a>
                                </li>
                            @endcan
                            @can('bank-deduction-employee-report.view')
                            <li class="nav-item">
                                <a href="{{ route('bank-deduction-employee-report.index') }}" class="nav-link" data-key="t-horizontal">Bank Deduction Employee Wise Report</a>
                            </li>
                            @endcan
                            @can('bank-deduction-report.view')
                            <li class="nav-item">
                                <a href="{{ route('bank-deduction-report.index') }}" class="nav-link" data-key="t-horizontal">Bank Deduction Report</a>
                            </li>
                            @endcan
                            @can('summary-department-report.view')
                            <li class="nav-item">
                                <a href="{{ route('summary-department-report.index') }}" class="nav-link" data-key="t-horizontal">Grand Summary Department Wise</a>
                            </li>
                            @endcan
                            @can('summary-ward-report.view')
                            <li class="nav-item">
                                <a href="{{ route('summary-ward-report.index') }}" class="nav-link" data-key="t-horizontal">Grand Summary Ward Wise</a>
                            </li>
                            @endcan
                            @can('summary-report.view')
                            <li class="nav-item">
                                <a href="{{ route('summary-report.index') }}" class="nav-link" data-key="t-horizontal">Grand Summary Report</a>
                            </li>
                            @endcan
                            @can('pay-sheet-excel.view')
                            <li class="nav-item">
                                <a href="{{ route('pay-sheet-excel.index') }}" class="nav-link" data-key="t-horizontal">Pay Sheet Excel</a>
                            </li>
                            @endcan
                            @can('allowance-report.view')
                            <li class="nav-item">
                                <a href="{{ route('allowance-report.index') }}" class="nav-link" data-key="t-horizontal">Allowance Report</a>
                            </li>
                            @endcan
                            @can('deduction-report.view')
                            <li class="nav-item">
                                <a href="{{ route('deduction-report.index') }}" class="nav-link" data-key="t-horizontal">Deduction Report</a>
                            </li>
                            @endcan
                        </ul>
                    </div>
                </li>
                @endcan
            </ul>
        </div>
    </div>

    <div class="sidebar-background"></div>
</div>


<div class="vertical-overlay"></div>
